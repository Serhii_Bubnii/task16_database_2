CREATE DATABASE IF NOT EXISTS `bd_airport` DEFAULT CHARACTER SET utf8 ;
USE `bd_airport`;

SET FOREIGN_KEY_CHECKS = 0;
drop table if exists clients;
drop table if exists airports;
drop table if exists flights;
drop table if exists planes;
drop table if exists ticket_price;
drop table if exists tickets;
drop table if exists planning;
drop table if exists cancel;
drop table if exists departure;
drop table if exists arrival;
SET FOREIGN_KEY_CHECKS = 1;


CREATE TABLE IF NOT EXISTS `clients` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    `surname` VARCHAR(45) NOT NULL,
    `passport_code` VARCHAR(20) NOT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `airports` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `nume` VARCHAR(45) NOT NULL,
    `code_iata` CHAR(3) DEFAULT NULL,
    `code_icao` CHAR(4) DEFAULT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `flights` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `flight_number` VARCHAR(20),
    `id_airport` INT NOT NULL,
    INDEX `FK_id_airport_idx` (`id_airport` ASC),
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_id_airport` FOREIGN KEY (`id_airport`)
        REFERENCES airports (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `planes` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(45) NOT NULL,
    `date_release` DATE NOT NULL,
    `number_seats` SMALLINT(3) NOT NULL,
    `board_number` VARCHAR(20) NOT NULL,
    `type_cabin` ENUM('Economy', 'Business', 'First') NOT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `ticket_price` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `ticket_price` DECIMAL(20 , 2 ) NOT NULL,
    `id_flight` INT NOT NULL,
    `id_planes` INT NOT NULL,
    INDEX `FK_id_flight_idx` (`id_flight` ASC),
    INDEX `FK_id_planes_idx` (`id_planes` ASC),
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_id_flight` FOREIGN KEY (`id_flight`)
        REFERENCES flights (`id`),
    CONSTRAINT `FK_id_planes` FOREIGN KEY (`id_planes`)
        REFERENCES planes (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `tickets` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `code_ticket` VARCHAR(20) NOT NULL,
    `id_client` INT NOT NULL,
    `id_ticket_price` INT NOT NULL,
    INDEX `FK_id_client_idx` (`id_client` ASC),
    INDEX `FK_id_ticket_price_idx` (`id_client` ASC),
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_id_client` FOREIGN KEY (`id_client`)
        REFERENCES clients (`id`),
    CONSTRAINT `FK_id_ticket_price` FOREIGN KEY (`id_ticket_price`)
        REFERENCES ticket_price (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `planning` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `departures` DATETIME NOT NULL,
    `flight_duration` SMALLINT(3) NOT NULL,
    `id_plane` INT NOT NULL,
    `id_flights` INT NOT NULL,
    INDEX `FK_id_flights_idx` (`id_flights` ASC),
    INDEX `FK_id_plane_idx` (`id_plane` ASC),
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_id_plane` FOREIGN KEY (`id_plane`)
        REFERENCES planes (`id`),
    CONSTRAINT `FK_id_flights` FOREIGN KEY (`id_flights`)
        REFERENCES flights (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `departure` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `departure_date` DATE NOT NULL,
    `id_planning` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_departure_planning_idx` (`id_planning` ASC),
    CONSTRAINT `fk_departure_planning` FOREIGN KEY (`id_planning`)
        REFERENCES `planning` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `arrival` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `time_arrival` DATETIME NOT NULL,
    `id_departure` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_arrival_departure_idx` (`id_departure` ASC),
    CONSTRAINT `fk_arrival_departure` FOREIGN KEY (`id_departure`)
        REFERENCES `departure` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `cancel` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `time_cancellation` DATETIME NOT NULL,
    `reason` TINYTEXT DEFAULT NULL,
    `id_planning` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_cancel_planning_idx` (`id_planning` ASC),
    CONSTRAINT `fk_cancel_planning` FOREIGN KEY (`id_planning`)
        REFERENCES `planning` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB;
